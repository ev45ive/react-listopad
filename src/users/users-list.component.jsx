import React from 'react'

export const UsersList = ({ users = [], onSelect, selected }) =>
  <div className="list-group">
    {users.map( user => 
      <div key={user.id}
      onClick={e => onSelect(user.id)}
      className={
        `list-group-item ${(selected == user.id) && 'active'}`
      }>
          {user.name}
      </div>
    )}
  </div>