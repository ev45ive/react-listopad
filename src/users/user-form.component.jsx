import React from 'react'

export class UserForm extends React.Component {
  state = {
    user: null
  }

  componentWillReceiveProps(newProps){
    this.setState({
      user:newProps.user
    })
  }

  onUpdate= (e) => {
    let { name: fieldName, value, /* checked, type */ } = e.target
    // this.state.user[fieldName] = value
    // this.setState({})
    this.setState( prevState => ({
      user: {
        ...prevState.user,
        [ fieldName ]: value
      }
    }))
  }

  save = (e) =>{
    e.preventDefault()
    this.props.onSave(this.state.user)
  }

  render() {
    let user = this.state.user

    return user? <form
      onSubmit={this.save}
    >
      <div className="form-group">
        <label>Name:</label>
        <input type="text"
        name="name"
        onChange={this.onUpdate}
        value={user.name} className="form-control" />
      </div>
      <div className="form-group">
        <label>E-mail:</label>
        <input type="text" 
        name="email"
        onChange={this.onUpdate}
        value={user.email} className="form-control" />
      </div>
      <button>Save</button>
    </form> : 'Please select user'
  }
}