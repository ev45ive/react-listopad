import React from 'react'

// redux middleware
// redux-thunk
// redux-promise
// redux-saga

import { UsersList } from './users-list.component'
import { UserForm } from './user-form.component'

import { bindActionCreators } from 'redux'
import { fetchUsers, userSelect, saveUser, getUser } from '../reducers/users.reducer'
import { connect } from 'react-redux'

const List = connect(
  state => ({
    users: state.users.list,
    selected: state.users.list.find( user => user.id == state.selectedId)
  }),
  dispatch => bindActionCreators({
    onSelect: userSelect
  },dispatch)
)(UsersList)

const Form = connect(
  state => ({
    users: state.users.list,
    selected: state.users.selected
  }),
  dispatch => bindActionCreators({ 
    onSave: saveUser(dispatch) 
  },dispatch),
  (state,actions,props) => ({
    ...props,
    user: state.users.find( user => user.id == state.selected),
    ...actions,
  })
)(UserForm)

class UsersLists extends React.Component {

  componentDidMount(){
    this.props.fetch()
  }

  render() {
    return <div>
      <div className="row">
        <div className="col">
          <h5>Users List</h5>
          <List/>
        </div>
        <div className="col">
          <h5>User Form</h5>
          <Form user={1} />
        </div>
      </div>
    </div>
  }
}

export const Users = connect(
  s=>s, dispatch => ({
    fetch: fetchUsers(dispatch)
  })
)(UsersLists)

