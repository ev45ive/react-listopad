import { createStore, combineReducers } from 'redux'
import { todos } from './reducers/todos.reducer'
import { users } from './reducers/users.reducer'

window.inc = (x = 1) => ({ type: 'INC', payload: x })
window.dec = (x = 1) => ({ type: 'DEC', payload: x })

const initialState = {
  value: 0
};

const counter = (state = 0,action) => {
  switch(action.type){
    case 'INC': return state + action.payload
    case 'DEC': return state - action.payload
  }
  return state
};

const rootReducer = combineReducers({
  todos,
  users,
  value: counter,
  nested: combineReducers({
    value: counter
  })
})

export const store = createStore(rootReducer, initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
// DEBUG:
store.subscribe(()=> console.log(store.getState()))
window.store = store