import React from 'react'
import { Todos } from './todos/todos.container'
import { connect } from 'react-redux'
import { getTodos, todoRemove, todoToggle, createTodo, updateTitle } from './reducers/todos.reducer'

const TodosControl = connect(
  state => ({
    value: state.todos.title
  }),
  dispatch => ({
    createTodo: title => dispatch(createTodo(title)),
    onChange: e => dispatch(updateTitle(e.target.value))
  })
)(props => <input type="text" className="form-control"
value={props.value}
onChange={props.onChange}
onKeyUp={event => event.keyCode == 13 && props.createTodo(props.value)}
ref={input => input && input.focus()}
/>)

export const TodosLists = (props => <div className="row">
  <div className="col">
    <Todos />
    <TodosControl />
  </div>
  <div className="col">
    <Todos />
  </div>
</div>)
