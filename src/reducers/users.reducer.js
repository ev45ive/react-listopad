// userSelect, saveUser, getUser 
import axios from 'axios'

export const fetchUsers = (dispatch) => () => {
  return axios.get('http://localhost:3000/users')
    .then(resp => resp.data)
    .then(users =>{
      dispatch({ type: 'USERS_LOADED', payload: users }) 
      return users; 
    })
    .catch(err => console.log('upss..', err))
}
window.fetchUsers = fetchUsers

export const saveUser = dispatch => (user) => {
  let url = 'http://localhost:3000/users/', request
  // Save user
  if (user.id) {
    request = axios.put(url + user.id, user)
  } else {
    request = axios.post(url, user)
  }
  request
    // Fetch list
    .then(resp => fetchUsers(dispatch)())
    .then(users =>{ 
      // Select saved user
      dispatch( userSelect(user.id) )
    })
  return ({ type: "FETCH_START" })
}

export const userSelect = selectedId => (
  {type:'USER_SELECT', payload: selectedId}
)

const initialState = {
  list:[]
}

export const users = (state = initialState,action)=>{

  switch(action.type){
    case 'USERS_LOADED': return {
      ...state,
      list: action.payload
    }
    case 'USER_SELECT': return {
      ...state,
      selected: action.payload
    }
  }

  return state
}