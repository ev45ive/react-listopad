// redux-actions
// redux-filter
// redux reselect

const initialState = {
  todos: [],
  title:''
}

// Action Creators :
export const todoAdd = (todo) => ({ 
  type: 'TODO_ADD', payload: todo
})
export const todoRemove = id => ({
  type: 'TODO_REMOVE', payload: id
})
export const todoToggle = (todo) => ({ 
  type: 'TODO_UPDATE', payload: {
    ...todo,
    completed: !todo.completed
  }
})

export const updateTitle = (payload) => ({
  type: 'UPDATE_TITLE', payload
})

export const createTodo = title => ({
  type: 'TODO_ADD', payload: {
    id: Date.now(),
    title,
    completed: false
  }
})

// export const todoToggle = id => ({
//   type:'TODO_TOGGLE',
//   payload: id
// })


const archiveCompleted = () => {
  this.setState((prevState) => ({
    todos: prevState.todos.filter(todo => !todo.completed),
    archived: [
      ...prevState.archived,
      ...prevState.todos.filter(todo => todo.completed)
    ]
  }))
}

// makeListReducer = listName => (state,action) =>{
//   if(action.meta.list !== listName) {return state}
// }

// Reducer
export const todos = (state = initialState, action) => {

  switch (action.type) {
    case 'UPDATE_TITLE': return {
      ...state,
      title: action.payload
    }
    case 'TODO_ADD': return {
      ...state, 
      todos: [...state.todos, action.payload ]
    }
    // case 'TODO_CREATE': return { 
    //   ...state,
    //   todos: [
    //     ...state.todos, {
    //       id: Date.now(), // <- side effect
    //       title: action.payload,
    //       completed: false
    //     }
    //   ]
    // }
    case 'TODO_REMOVE': return {
      ...state,
      todos: state.todos.filter(t => t.id != action.payload)
    }
    case 'TODO_UPDATE': return { 
      ...state,
      todos: state.todos.map(todo =>
        (todo.id != action.payload.id) ? todo : action.payload
      )
    }
    case 'TODO_TOGGLE': return { ...state,
      todos: state.todos.map(todo =>
        (todo.id != action.payload) ? todo : {
          ...todo,
          completed: !todo.completed
        }
      )
    }
  }

  return state
}

// Queries:
export const getTodos = state => state.todos;

export const getTodo = (id) => state => state.todos.find(
  todo => todo.id == id
)
export const getCompletedCount = state => {
  return state.todos.filter(todo => todo.completed).length
}