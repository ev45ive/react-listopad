import React from 'react'
import { render } from 'react-dom'
import './style.css'

import { Provider } from 'react-redux'
import { store } from './store.js'

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import { App } from './app'
 
render(
  <Router>
    <Provider store={store}>
          <App />
    </Provider>
</Router>,
document.getElementById('app-root') )

window.React = React