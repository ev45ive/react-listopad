import { TodosList } from './todos-list.component'
import React from 'react'
import { connect } from 'react-redux'

import { getTodos, todoRemove, todoToggle } from '../reducers/todos.reducer'

export const Todos = connect(
  // mapStateToProps
  state => ({
    title: 'Redux Todos!',
    todos: getTodos(state.todos)
  }),
  // mapDispatchToProps,
  dispatch => ({
    'onToggle': todo => dispatch(todoToggle(todo)),
    'onRemove': id => dispatch(todoRemove(id))
  })
)(TodosList)

// export class Todos extends React.PureComponent {

//    render() {
//     return <div>
//       <TodosList
//         title={this.props.title}
//         todos={this.state.todos}
//         onToggle={this.props.onToggle}
//         onRemove={this.props.onRemove}
//       />
//       {/* <p>Completed: {this.getCompletedCount()} </p> */}
//     </div>
//   }
// }